package Ref;

import java.time.LocalDate;
import java.util.Date;

public class EqInstrument extends Instrument {
    LocalDate exDividend; //

    //Added price parameter
    public EqInstrument(Ric ric, double price, LocalDate exDividend) {
        super(ric,price);
        this.exDividend=exDividend; //sets the date to be todays date for now
    }
    
    /**
	 * @return receivable - true when buyer receives dividends, false when
	 * seller receives dividends
	 */
    public boolean receiveDividends(LocalDate current) {
    	boolean receivable = false; //false if current is after or equal to exDividend
    	if(current.isBefore(this.exDividend)) {
    		receivable = true; //true if current is before exDividend
    	}
    	return receivable;
    }
}