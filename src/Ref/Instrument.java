package Ref;

import java.io.Serializable;
import java.util.Date;

public class Instrument implements Serializable {
    long id;
    String name;
    String isin;
    String sedol;
    String bbid;
    public Ric ric;
    //Added price variable
    private double price;

    //Added starting price to constructor
    public Instrument(Ric ric, double price) {
        this.ric = ric;
        this.price=price;
    }

    public String toString() {
        return ric.ric;
    }

    //Getter for price
    public double getPrice() {
        return price;
    }

    //Setter for price
    public void setPrice(double price){
        this.price=price;
    }
}