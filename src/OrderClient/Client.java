package OrderClient;

import OrderManager.Order;

import java.io.IOException;

public interface Client {
    //Outgoing messages
    int sendOrder(Object par0) throws IOException, InterruptedException;

    void sendCancel(int id, Order o) throws IOException, InterruptedException;

    //Incoming messages
    void partialFill(Order order);

    void fullyFilled(Order order);

    void cancelled(Order order);

    void messageHandler();
}