package OrderClient;

import Ref.Instrument;

import java.io.Serializable;

public class NewOrderSingle implements Serializable {
    public int size;
    public double price = 0;
    public Instrument instrument;
    public enum Status {Buy,Sell}
    public Status stat;

    public NewOrderSingle(int size, double price, Instrument instrument, Status stat) {
        this.size = size;
        this.price = price;
        this.instrument = instrument;
        this.stat=stat;
    }
    public NewOrderSingle(int size, Instrument instrument, Status stat) {
        this.size = size;
        this.instrument = instrument;
        this.stat=stat;
    }
}