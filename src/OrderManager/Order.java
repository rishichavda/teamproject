package OrderManager;

import java.io.Serializable;
import java.util.ArrayList;

import OrderClient.NewOrderSingle.Status;
import Ref.Instrument;

public class Order implements Serializable {
	public Instrument instrument;
	public double initialMarketPrice;
	public int id; // TODO these should all be longs
    public long clientOrderID;
    public Status stat;
	int bestPriceCount;
	int clientid;
	private int size;
	double[] bestPrices;
	char ordStatus; // OrdStatus is Fix 39, 'A' is 'Pending New'
	short orderRouter;
	ArrayList<Order> slices;
	private ArrayList<Fill> fills;

	public Order(int clientId, long ClientOrderID, Instrument instrument, int size, Status stat) {
		/**
		 * used for market orders
		 */
		this.clientid = clientId;
		this.clientOrderID = ClientOrderID;
		this.instrument = instrument;
		this.size = size;
		fills = new ArrayList<>();
		slices = new ArrayList<>();
		ordStatus = 'A';
		this.stat = stat;
	}
	public Order(int clientId, long ClientOrderID, Instrument instrument, int size, Status stat, double price) {
		/**
		 * used for limit orders
		 */
		this.clientid = clientId;
		this.clientOrderID = ClientOrderID;
		this.instrument = instrument;
		this.size = size;
		fills = new ArrayList<>();
		slices = new ArrayList<>();
		ordStatus = 'A';
		this.stat = stat;
		this.initialMarketPrice = price;
	}

	int newSlice(int sliceSize) {
		slices.add(new Order(id, clientOrderID, instrument, sliceSize, stat, initialMarketPrice));
		return slices.size() - 1;
	}
	
	int sliceSizes() {
		int totalSizeOfSlices = 0;
		for (Order c : slices)
			totalSizeOfSlices += c.size; // adding slices together
		return totalSizeOfSlices;
	}

	private int sizeFilled() {
		int filledSoFar = 0;
		for (Fill f : fills) {
			filledSoFar += f.size;
		}
		for (Order c : slices) {
			filledSoFar += c.sizeFilled();
		}
		return filledSoFar;
	}

	public int sizeRemaining() {
		return size - sizeFilled();
	}

	// Status state;
	float price() {
		//TODO: might be working? who really knows
		float sum = 0;
		float sizeTotal = 0;
		for (Fill fill : fills) {
			sum += fill.price;
		}
		sizeTotal+=fills.size();
		for(Order a:slices){
			for(Fill f:a.fills){
				sum += f.price;
			}
			sizeTotal+=fills.size();
		}
		return sum / sizeTotal;
	}

	//Added clientOrderID to fills (each constructor reference)
	void createFill(int size, double price, long cOID) {
		fills.add(new Fill(size, price, cOID));
		if (sizeRemaining() == 0) {
			ordStatus = '2';
		} else {
			ordStatus = '1';
		}
	}

	void cross(Order matchingOrder) { // matching order should occur for relevant prices
		// pair slices first and then parent
		for (Order slice : slices) {
			if (slice.sizeRemaining() == 0)
				continue;
			// TODO could optimise this to not start at the beginning every time
			for (Order matchingSlice : matchingOrder.slices) {
				int msze = matchingSlice.sizeRemaining(); // size of slices left of matching order
				if (msze == 0)
					continue; // if no slices left in order
				int sze = slice.sizeRemaining(); // size of slices left of this order
				if (sze <= msze) { // if there are more slices in this order than the matching order
					slice.createFill(sze, initialMarketPrice,clientOrderID); // fill this slice of this slice size
					matchingSlice.createFill(sze, initialMarketPrice,clientOrderID); // fill matching order of this slice size
					break;
				}
				else {
					slice.createFill(msze, initialMarketPrice,clientOrderID);
					matchingSlice.createFill(msze, initialMarketPrice,clientOrderID);
				}
				
			}

			int sze = slice.sizeRemaining();
			int mParent = matchingOrder.sizeRemaining() - matchingOrder.sliceSizes();

			if (sze > 0 && mParent > 0) {
				if (sze >= mParent) {
					slice.createFill(sze, initialMarketPrice,clientOrderID);
					matchingOrder.createFill(sze, initialMarketPrice,clientOrderID);
				} else {
					slice.createFill(mParent, initialMarketPrice,clientOrderID);
					matchingOrder.createFill(mParent, initialMarketPrice,clientOrderID);
				}
			}
			// no point continuing if we didn't fill this slice, as we must already have
			// fully filled the matchingOrder
			if (slice.sizeRemaining() > 0)
				break;
		}

		if (sizeRemaining() > 0) {
			for (Order matchingSlice : matchingOrder.slices) {
				int msze = matchingSlice.sizeRemaining();
				if (msze == 0)
					continue;
				int sze = sizeRemaining();
				if (sze <= msze) {
					createFill(sze, initialMarketPrice,clientOrderID);
					matchingSlice.createFill(sze, initialMarketPrice,clientOrderID);
					break;
				}
				// sze>msze
				createFill(msze, initialMarketPrice,clientOrderID);
				matchingSlice.createFill(msze, initialMarketPrice,clientOrderID);
			}
			int sze = sizeRemaining();
			int mParent = matchingOrder.sizeRemaining() - matchingOrder.sliceSizes();
			if (sze > 0 && mParent > 0) {
				if (sze >= mParent) {
					createFill(sze, initialMarketPrice,clientOrderID);
					matchingOrder.createFill(sze, initialMarketPrice,clientOrderID);
				} else {
					createFill(mParent, initialMarketPrice,clientOrderID);
					matchingOrder.createFill(mParent, initialMarketPrice,clientOrderID);
				}
			}
		}
	}
	
	// RC - change all slices to cancelled
	void cancel() {
		// state=cancelled
    	for(Order s : slices) {
    		ordStatus = 'F';
    	}
	}
	
	public void setOrdStatus(char newOS) {
		this.ordStatus = newOS;
	}
}

class Basket {
	Order[] orders;
}

class Fill implements Serializable {
	//Added clientOrderID so fills can be linked to orders
	long clientOrderID;
	int size;
	double price;

	//Added in an ID for fills
	Fill(int size, double price, long clientOrderID) {
		this.clientOrderID = clientOrderID;
		this.size = size;
		this.price = price;
	}
}
