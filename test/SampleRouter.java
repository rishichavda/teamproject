import OrderRouter.Router;
import Ref.Instrument;
import Ref.Ric;

import javax.net.ServerSocketFactory;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Random;

public class SampleRouter extends Thread implements Router {
    private static final Random RANDOM_NUM_GENERATOR = new Random();
    private static final Instrument[] INSTRUMENTS = {
            new Instrument( new Ric( "VOD.L" ), 170.00 ),
            new Instrument( new Ric( "BP.L" ), 562.00 ),
            new Instrument( new Ric( "BT.L" ), 228.00 )};
    ObjectInputStream is;
    ObjectOutputStream os;
    private Socket omConn;
    private int port;

    public SampleRouter(String name, int port) {
        this.setName( name );
        this.port = port;
    }

    public void run() {
        //OM will connect to us
        try {
            omConn = ServerSocketFactory.getDefault().createServerSocket( port ).accept();
            while (true) {
                if (0 < omConn.getInputStream().available()) {
                    is = new ObjectInputStream( omConn.getInputStream() );
                    Router.api methodName = (Router.api) is.readObject();
                    System.out.println( "Order Router recieved method call for:" + methodName );
                    switch (methodName) {
                        case routeOrder:
                            routeOrder( is.readInt(), is.readInt(), is.readInt(), (Instrument) is.readObject() );
                            break;
                        case priceAtSize:
                            priceAtSize( is.readInt(), is.readInt(), (Instrument) is.readObject(), is.readInt() );
                            break;
                        case sendCancel:
                            sendCancel( is.readInt(), is.readInt(), is.readInt(), (Instrument) is.readObject() );
                        default:
                            System.out.println("Unknown method call to router: "+methodName);
                    }
                } else {
                    Thread.sleep( 100 );
                }
                //TODO check if information is routed back to the OrderManager
                //omConn.c
            }
        } catch (IOException | ClassNotFoundException | InterruptedException e) {
            // TODO Auto-generated catch block > Do something useful with it.
            e.printStackTrace();
        }
    }

    @Override
    public void routeOrder(int id, int sliceId, int size, Instrument i) throws IOException, InterruptedException { //MockI.show(""+order);
        int fillSize = RANDOM_NUM_GENERATOR.nextInt( size );
        double fillPrice = Math.round( i.getPrice() + (Math.random() >= 0.5 ? +1.00 : -1.00) );
        Thread.sleep( 42 );
        os = new ObjectOutputStream( omConn.getOutputStream() );
        os.writeObject( "newFill" );
        os.writeInt( id );
        os.writeInt( sliceId );
        os.writeInt( fillSize );
        os.writeDouble( fillPrice );
        System.out.println( "routeOrder Price: " +
                Double.toString( Math.round( fillPrice ) ) );
        os.flush();
    }

    @Override
    public void sendCancel(int id, int sliceId, int size, Instrument i) throws IOException { //MockI.show(""+order);
        os = new ObjectOutputStream( omConn.getOutputStream() );
        os.writeObject( "cancel" );
        os.writeInt( id );
        os.writeInt( sliceId );
        os.writeInt( size );
        os.writeObject( i );
        os.flush();
    }

    @Override
    public void priceAtSize(int id, int sliceId, Instrument i, int size) throws IOException {
        os = new ObjectOutputStream( omConn.getOutputStream() );
        double pas = Math.round( i.getPrice() + (Math.random() >= 0.50 ? +1.00 : -1.00) );
        os.writeObject( "bestPrice" );
        os.writeInt( id );
        os.writeInt( sliceId );
        os.writeDouble( pas );
        System.out.println( "Price at size: " +
                Double.toString( pas ) );
        os.flush();
    }
}
