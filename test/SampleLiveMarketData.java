import static OrderClient.NewOrderSingle.Status.Buy;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import LiveMarketData.LiveMarketData;
import OrderClient.NewOrderSingle;
import OrderManager.Order;
import Ref.Instrument;

public class SampleLiveMarketData implements LiveMarketData{
	public void setPrice(Order o){
		double priceBuyUF;
		double priceBuy;
		double priceSellUF;
		double priceSell;
		double alteration = ThreadLocalRandom.current().nextDouble(-0.0005, 0.0005);
		
		if(o.stat.equals(Buy)){
			//Get a price within 0.2% of last used price for that instrument. Format to 3 decimal points
			priceBuyUF = o.instrument.getPrice()+(o.instrument.getPrice()*0.002);
			priceBuy = Double.valueOf(String.format("%1.1f", priceBuyUF));
			o.instrument.setPrice(priceBuy);
			o.initialMarketPrice = priceBuy;
		}else{
			//Get a price within 0.2% of last used price for that instrument. Format to 3 decimal points
			priceSellUF =o.instrument.getPrice()+(o.instrument.getPrice()*(0.002+alteration));
			priceSell = Double.valueOf(String.format("%1.1f", priceSellUF));
			o.instrument.setPrice(priceSell);
			o.initialMarketPrice = priceSell;
		}
		
	}
}
